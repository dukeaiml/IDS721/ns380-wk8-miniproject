use clap::Parser;
use polars::prelude::*;
use std::error::Error;

// Search for a pattern in a file and display the lines that contain it.
#[derive(Parser)]
struct Cli {
    // The year to get stats from
    year: i32,
    // The player to get stats for
    player: String,
}

fn get_stats(year: i32, player: String) -> Result<(), Box<dyn Error>> {
    if year < 2020 || year > 2024 {
        Err(format!("Unsupported year {}, year must be between 2020 and 2024 (inclusive)", year))?;
    }
    if player == "" {
        Err("Player name must be specified")?;
    }
    if !player.contains(" ") {
        Err("Player name must be specified as first name and last name")?;
    }
    let filename = format!("data/stats-{}.csv", year);
    let q = LazyCsvReader::new(filename)
        .has_header(true)
        .finish()?
        .select([col("Player"), col("G"), col("MP"), col("FGA"), col("FG%"), col("3PA"), col("3P%"), col("eFG%"), col("TRB"), col("AST"), col("STL"), col("BLK"), col("TOV"), col("PTS")])
        .filter(col("Player").eq(lit(player.clone())));

    let df = q.collect()?.unique(Some(&["Player".to_string()]), UniqueKeepStrategy::First, None)?;
    if df.height()==0 {
        Err(format!("No player named {}", player))?;
    }
    std::env::set_var("POLARS_FMT_MAX_COLS", "14");
    println!("{:?}", df);
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Cli::parse();
    return get_stats(args.year, args.player);
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_get_stats_good_input() {
        assert!(!get_stats(2022, "LeBron James".to_owned()).is_err());
    }
    
    #[test]
    fn test_get_stats_bad_year() {
        assert!(get_stats(2010, "LeBron James".to_owned()).is_err());
    }
    
    #[test]
    fn test_get_stats_bad_name() {
        assert!(get_stats(2022, "LeBron".to_owned()).is_err());
    }
}
